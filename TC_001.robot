*** Settings ***
Library  RequestsLibrary

*** Variables ***

${Base_URL}  http://thetestingworldapi.com/
${StudentID}  45406

*** Test Cases ***
Get_DataSources

#    Obtain DB Type
    create session  Get_Data_Sources  ${Base_URL}
    
    ${response}=  get request  Get_Data_Sources  api/studentsDetails

    log to console  ${response.status_code}
    
    ${response1}=  get request  Get_Data_Sources  api/studentsDetails/${StudentID}
    log to console  ${response1.content}

#    Validations


*** Keywords ***
